<?php

class area {
    //Função que retorna o númerode quadrados de uma área
    public function CalculaArea($numero) {
        
        //Verifica se o numero é maior que 1 caso for faz o calculo do número de quadrados
        if ($numero > 1){
            //Retorna o último numero antes do informado
            for($i = 1; $i < $numero; $i++){
                $numeroAnterior = $i;
            }
            //Formúla para calcular o numero de quadrados da área
            $numeroQuadrados = (($numeroAnterior * $numeroAnterior) + ($numero* $numero));
            
        }else{
            $numeroQuadrados = 1;
            
        }
       
        return $numeroQuadrados;;
    }
}
