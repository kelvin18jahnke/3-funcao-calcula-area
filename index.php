<?php
    require_once './funcoes/area.php';
    
    $area = new area();
    
    if (isset($_POST['valor'])) {
        
        $valor =  $_POST['valor'];
        
        if($valor  == ''){
            echo '<div class="alert alert-info" role="alert">
                Informe um número para retornar o número de quadrados!
                </div>';
            
        }else{
            //Funnção que retorna o número de quadrados
            $numeroQuadrados = $area->CalculaArea($valor);  
        }
    }else{
        $valor = '';
    }
    
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
        <title>Área:</title>
    </head>
    <body>
       
        <form action="index.php" method="POST">
            <h1>Função que Retorna a quantidade de quadrados que compoem uma Área</h1>
            <div class="row g-3 align-items-center" style="margin: 2% 10%;">
                <div class="col-auto">
                    <label  class="col-form-label">Informe um número da Área:</label>
                </div>
                <div class="col-auto">
                    <input type="number" name="valor" value="<?=$valor?>" class="form-control">
                </div>
                <div class="col-auto">
                    <span  class="form-text">
                       <?php
                        if(isset($numeroQuadrados) && $numeroQuadrados != ''){
                            echo 'O número de quadrados é : ' . $numeroQuadrados;
                        } 
                       ?>
                    </span>
                </div>
                <div class="col-auto">
                    <button type="submit" class="btn btn-primary">Consultar</button>
                </div>
            </div>
        </form>
    </body>
</html>
